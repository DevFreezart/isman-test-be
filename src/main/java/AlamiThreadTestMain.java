import dto.Eod;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import threads.AverageThread;
import threads.BonusBalanceThread;
import threads.FreeTransferThread;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class AlamiThreadTestMain {

    private static final Logger log = LoggerFactory.getLogger(AlamiThreadTestMain.class);

    private static final String FOLDER_NAME = "csv";
    private static final String[] HEADERS = {"id", "Nama", "Age", "Balanced", "Previous Balanced", "Average Balanced", "Free Transfer"};
    private static final String BEFORE_EOD_FILE = "Before Eod.csv";
    private static final String AFTER_EOD_FILE = "After Eod.csv";
    private static final String NO1_FILE_TEMP = "no1temp.csv";
    private static final String NO2_FILE_TEMP = "no2temp.csv";
    private static final String NO3_FILE_TEMP = "no3temp.csv";

    public static void main(String[] args) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        soalNo1();
        soalNo2();
        soalNo3();
        buildAfterEod();
        stopWatch.stop();
        log.info("Executed in: {}", stopWatch.getTime(TimeUnit.MILLISECONDS) + "ms");
    }

    public static void soalNo1() {

        ExecutorService executorService = Executors.newFixedThreadPool(10);

        try (
                FileWriter writer = new FileWriter(FOLDER_NAME + "/" + NO1_FILE_TEMP);
                CSVPrinter printer = new CSVPrinter(writer, CSVFormat.DEFAULT.withDelimiter(';').withHeader(HEADERS));
        ) {

            List<Eod> eodList = readFile(BEFORE_EOD_FILE);
            executorService.invokeAll(
                    eodList.stream()
                            .map(data ->
                                    new AverageThread(printer, data)
                            )
                            .collect(toList()));


            executorService.shutdownNow();

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

    }

    public static void soalNo2() {

        ExecutorService executorService = Executors.newFixedThreadPool(10);

        try (
                FileWriter writer = new FileWriter(FOLDER_NAME + "/" + NO2_FILE_TEMP);
                CSVPrinter printer = new CSVPrinter(writer, CSVFormat.DEFAULT.withDelimiter(';').withHeader(HEADERS));
        ) {

            List<Eod> eodList = readFile(NO1_FILE_TEMP);
            executorService.invokeAll(
                    eodList.stream()
                            .map(data ->
                                    new FreeTransferThread(printer, data)
                            )
                            .collect(toList()));


            executorService.shutdownNow();

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

    }

    public static void soalNo3() {

        ExecutorService executorService = Executors.newFixedThreadPool(8);

        try (
                FileWriter writer = new FileWriter(FOLDER_NAME + "/" + NO3_FILE_TEMP);
                CSVPrinter printer = new CSVPrinter(writer, CSVFormat.DEFAULT.withDelimiter(';').withHeader(HEADERS));
        ) {

            List<Eod> eodList = readFile(NO2_FILE_TEMP);
            executorService.invokeAll(
                    eodList.stream()
                            .map(data ->
                                    new BonusBalanceThread(printer, data)
                            )
                            .collect(toList()));


            executorService.shutdownNow();

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

    }

    private static List<Eod> readFile(String fileName) {

        try (
                Reader reader = new FileReader(FOLDER_NAME + "/" + fileName);
                CSVParser records = CSVFormat.newFormat(';').withFirstRecordAsHeader().parse(reader);
        ) {
            List<Eod> eodList = records.getRecords()
                    .stream()
                    .map(
                            data -> {
                                Eod eod = new Eod();
                                eod.setId(data.get("id"));
                                eod.setNama(data.get("Nama"));
                                eod.setAge(Integer.valueOf(data.get("Age")));
                                eod.setBalance(new BigDecimal(data.get("Balanced")));
                                eod.setPreviousBalanced(new BigDecimal(data.get("Previous Balanced")));
                                eod.setAverageBalanced(new BigDecimal(data.get("Average Balanced")));
                                eod.setFreeTransfer(Integer.valueOf(data.get("Free Transfer")));
                                return eod;
                            }
                    ).collect(Collectors.toList());

            return eodList;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ArrayList<>();
        }

    }

    public static void buildAfterEod() {

        try (
                FileWriter writer = new FileWriter(FOLDER_NAME + "/" + AFTER_EOD_FILE);
                CSVPrinter printer = new CSVPrinter(writer, CSVFormat.DEFAULT.withDelimiter(';').withHeader(HEADERS));
        ) {

            List<Eod> eodList = readFile(NO2_FILE_TEMP);

            eodList.stream()
                    .sorted(Comparator.comparingInt(a -> Integer.valueOf(a.getId())))
                    .forEach(
                            data -> {
                                try {
                                    printer.printRecord(data.getId(), data.getNama(), data.getAge(), data.getBalance(), data.getPreviousBalanced(), data.getAverageBalanced(), data.getFreeTransfer());
                                } catch (IOException e) {
                                    System.out.println(e.getMessage());
                                }
                            }
                    );

            FileUtils.deleteQuietly(new File(FOLDER_NAME + "/" + NO1_FILE_TEMP));
            FileUtils.deleteQuietly(new File(FOLDER_NAME + "/" + NO2_FILE_TEMP));
            FileUtils.deleteQuietly(new File(FOLDER_NAME + "/" + NO3_FILE_TEMP));

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

    }

}
