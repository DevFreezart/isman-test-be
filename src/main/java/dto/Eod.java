package dto;

import java.math.BigDecimal;

public class Eod {

    String id;

    String nama;

    Integer age;

    BigDecimal balance;

    BigDecimal previousBalanced;

    BigDecimal averageBalanced;

    Integer freeTransfer;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getPreviousBalanced() {
        return previousBalanced;
    }

    public void setPreviousBalanced(BigDecimal previousBalanced) {
        this.previousBalanced = previousBalanced;
    }

    public BigDecimal getAverageBalanced() {
        return averageBalanced;
    }

    public void setAverageBalanced(BigDecimal averageBalanced) {
        this.averageBalanced = averageBalanced;
    }

    public Integer getFreeTransfer() {
        return freeTransfer;
    }

    public void setFreeTransfer(Integer freeTransfer) {
        this.freeTransfer = freeTransfer;
    }

    @Override
    public String toString() {
        return "dto.Eod{" +
                "id='" + id + '\'' +
                ", nama='" + nama + '\'' +
                ", age=" + age +
                ", balance=" + balance +
                ", previousBalanced=" + previousBalanced +
                ", averageBalanced=" + averageBalanced +
                ", freeTransfer=" + freeTransfer +
                '}';
    }
}
