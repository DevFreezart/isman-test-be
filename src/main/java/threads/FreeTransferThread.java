package threads;

import dto.Eod;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.concurrent.Callable;

public class FreeTransferThread implements Callable<Eod> {

    private static final Logger log = LoggerFactory.getLogger(FreeTransferThread.class);

    CSVPrinter printer;

    Eod data;

    public FreeTransferThread(CSVPrinter printer, Eod data) {
        this.printer = printer;
        this.data = data;
    }

    @Override
    public Eod call() throws Exception {
        log.info("No 2 Thread-No: " + Thread.currentThread().getName());
        if (data.getAverageBalanced().compareTo(new BigDecimal(100)) >= 0 && data.getAverageBalanced().compareTo(new BigDecimal(150)) <= 0) {
            data.setFreeTransfer(5);
        } else if (data.getAverageBalanced().compareTo(new BigDecimal(150)) >= 1) {
            data.setFreeTransfer(data.getFreeTransfer()+25);
        }
        synchronized (printer) {
            printer.printRecord(data.getId(), data.getNama(), data.getAge(), data.getBalance(), data.getPreviousBalanced(), data.getAverageBalanced(), data.getFreeTransfer());
        }
        return data;
    }

}
