package threads;

import dto.Eod;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.concurrent.Callable;

public class BonusBalanceThread implements Callable<Eod> {

    private static final Logger log = LoggerFactory.getLogger(BonusBalanceThread.class);

    CSVPrinter printer;

    Eod data;

    public BonusBalanceThread(CSVPrinter printer, Eod data) {
        this.printer = printer;
        this.data = data;
    }

    @Override
    public Eod call() throws Exception {
        log.info("No 3 Thread-No: " + Thread.currentThread().getName());

        if (Integer.valueOf(data.getId()) <=100) {
            data.setBalance(data.getBalance().add(new BigDecimal(10)));
        }
        synchronized (printer) {
            printer.printRecord(data.getId(), data.getNama(), data.getAge(), data.getBalance(), data.getPreviousBalanced(), data.getAverageBalanced(), data.getFreeTransfer());
        }
        return data;
    }
}
