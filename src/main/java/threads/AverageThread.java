package threads;

import dto.Eod;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.concurrent.Callable;

public class AverageThread implements Callable<Eod> {

    private static final Logger log = LoggerFactory.getLogger(AverageThread.class);

    CSVPrinter printer;

    Eod data;

    public AverageThread(CSVPrinter printer, Eod data) {
        this.printer = printer;
        this.data = data;
    }

    @Override
    public Eod call() throws Exception {
        log.info("No 1 Thread-No: " + Thread.currentThread().getName());
        data.setAverageBalanced((data.getBalance().add(data.getPreviousBalanced())).divide(new BigDecimal(2)));
        synchronized (printer) {
            printer.printRecord(data.getId(), data.getNama(), data.getAge(), data.getBalance(), data.getPreviousBalanced(), data.getAverageBalanced(), data.getFreeTransfer());
        }
        return data;
    }

}
